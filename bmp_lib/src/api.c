//
// Created by vlad on 1/4/23.
//
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "../include/file.h"
#include "../include/image.h"
#include "../include/io_statuses.h"
#include "../include/api.h"
#include "../../include/utils.h"

void read_from_file(struct image* img, const char* path, enum read_status (reader)(FILE* in, struct image* img)) {
    FILE* in = NULL;
    enum open_file_status of = open_file(&in, "rb", path);
    if (of != OPEN_SUCCESS)
        throw_exception("Opening file", of);
    enum read_status status = reader(in, img);
    if (status != READ_OK)
        throw_exception("Reading file", status);
    close_file(&in);
}

void write_to_file(struct image* img, const char* path, enum write_status (writer)(FILE* in, const struct image* img)) {
    FILE* out = NULL;
    enum open_file_status ofs = open_file(&out, "wb", path);
    if (ofs != OPEN_SUCCESS)
        throw_exception("Opening file", ofs);
    enum write_status ws = writer(out, img);
    if (ws != WRITE_OK)
        throw_exception("Writing file", ws);
    close_file(&out);
}