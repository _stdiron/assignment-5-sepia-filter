//
// Created by vlad on 1/4/23.
//

#include <sys/time.h>
#include <sys/resource.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "../bmp_lib/include/image.h"
#include "../include/utils.h"

bool parse_params(int argc, char** argv, char** src_path, char** out_path, char** out_path2) {
    if (argc != 4) {
        return false;
    }
    *src_path = argv[1];
    *out_path = argv[2];
    *out_path2 = argv[3];
    return true;
}

void throw_exception(const char* message, uint64_t code) {
    perror("Errno error message ");
    if (message != NULL)
        printf("Internal info: %s; code: %ld\n", message, code);
    exit(1);
}

struct image test_with_timer(struct image (test_func)(struct image const source), struct image source, FILE* out) {
    struct rusage r;
    struct timeval start;
    struct timeval end;

    getrusage(RUSAGE_SELF, &r );
    start = r.ru_utime;

    struct image result = test_func(source);

    getrusage(RUSAGE_SELF, &r );
    end = r.ru_utime;
    long time = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
    fprintf(out, "Time elapsed in microseconds: %ld\n", time );

    return result;
}