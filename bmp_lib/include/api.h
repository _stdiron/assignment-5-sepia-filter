//
// Created by vlad on 1/4/23.
//

#ifndef ASSIGNMENT_5_SEPIA_FILTER_API_H
#define ASSIGNMENT_5_SEPIA_FILTER_API_H

void read_from_file(struct image* img, const char* path, enum read_status (reader)(FILE* in, struct image* img));
void write_to_file(struct image* img, const char* path, enum write_status (writer)(FILE* in, const struct image* img));

#endif //ASSIGNMENT_5_SEPIA_FILTER_API_H
