section .rodata

align 16
red_mult: dd 0.272, 0.349, 0.393, 0.272
green_mult: dd 0.534, 0.686, 0.769, 0.543
blue_mult: dd 0.131, 0.168, 0.189, 0.131
max_values: dd 255, 255, 255, 255

%macro clear_regs 0
    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2
%endmacro

%macro set_pixels 0
    ; blue
    pinsrb xmm0, [rdi], 0 ; pixel 1
    pinsrb xmm0, [rdi + 3], 12 ; pixel 2
    ; green
    pinsrb xmm1, [rdi+1], 0 ; pixel 1
    pinsrb xmm1, [rdi + 3 + 1], 12 ; pixel 2
    ; red
    pinsrb xmm2, [rdi + 2], 0 ; pixel 1
    pinsrb xmm2, [rdi + 3 + 2], 12 ; pixel 2
%endmacro

%macro shuffle_pixels 1
        %if %1 == 1
            %define shuffle_pattern 0b11000000
        %elif %1 == 2
            %define shuffle_pattern 0b11110000
        %else
            %define shuffle_pattern 0b11111100
        %endif
        shufps xmm0, xmm0, shuffle_pattern
        shufps xmm2, xmm2, shuffle_pattern
        shufps xmm1, xmm1, shuffle_pattern
%endmacro

%macro to_floats 0
    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2
%endmacro

%macro calculate_sepia 0
    ; sepia formula
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    addps xmm0, xmm1
    addps xmm0, xmm2
    ; convert and check for maximum value
    cvtps2dq xmm0, xmm0
    pminud xmm0, [max_values]
%endmacro

%macro save 0
    pextrb [rsi], xmm0, 0
    pextrb [rsi+1], xmm0, 4
    pextrb [rsi+2], xmm0, 8
    pextrb [rsi+3], xmm0, 12
%endmacro

%macro next_iter 0
    add rsi, 4
    add rdi, 3
    ; b | r | g | b -> g | b | r | g -> r | g | b | r
    ; shuffle multipliers
    shufps xmm3, xmm3, 0b01001001
    shufps xmm4, xmm4, 0b01001001
    shufps xmm5, xmm5, 0b01001001
%endmacro

section .text
GLOBAL process_pixels_sse

; rdi = from, rsi = out
process_pixels_sse:
    ; load multipliers matrix
    movaps xmm3, [blue_mult]
    movaps xmm4, [green_mult]
    movaps xmm5, [red_mult]

    clear_regs
    set_pixels
    shuffle_pixels 1
    to_floats
    calculate_sepia
    save
    next_iter

    clear_regs
    set_pixels
    shuffle_pixels 2
    to_floats
    calculate_sepia
    save
    next_iter

    clear_regs
    set_pixels
    shuffle_pixels 3
    to_floats
    calculate_sepia
    save

    ret