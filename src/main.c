//
// Created by vlad on 12/30/22.
//

#include <stdio.h>
#include "utils.h"
#include "test.h"


int main( int argc, char** argv ) {
    performance_test("../tests/image1");
    performance_test("../tests/image2");
    return 0;
}

