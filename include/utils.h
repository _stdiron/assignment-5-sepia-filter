//
// Created by vlad on 1/4/23.
//

#ifndef ASSIGNMENT_5_SEPIA_FILTER_UTILS_H
#define ASSIGNMENT_5_SEPIA_FILTER_UTILS_H

#include <stdbool.h>
#include <stdint.h>

bool parse_params(int argc, char** argv, char** src_path, char** out_path, char** out_path2);
void throw_exception(const char* message, uint64_t code);
struct image test_with_timer(struct image (test_func)(struct image const source), struct image source, FILE* out);

#endif //ASSIGNMENT_5_SEPIA_FILTER_UTILS_H
