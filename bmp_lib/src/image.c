//
// Created by vlad on 11/6/22.
//

#include <stdlib.h>

#include "../include/image.h"

struct image create_image(uint64_t width, uint64_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
}

void clear_image_memory(struct image* img) {
    img->width = 0;
    img->height = 0;
    if (img->data)
        free(img->data);
}
