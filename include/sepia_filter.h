//
// Created by vlad on 1/4/23.
//

#ifndef ASSIGNMENT_5_SEPIA_FILTER_SEPIA_FILTER_H
#define ASSIGNMENT_5_SEPIA_FILTER_SEPIA_FILTER_H

struct image sepia_filter(struct image source);
struct image sepia_filter_sse(struct image source);

#endif //ASSIGNMENT_5_SEPIA_FILTER_SEPIA_FILTER_H
