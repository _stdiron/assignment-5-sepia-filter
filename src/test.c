//
// Created by vlad on 1/24/23.
//

#include <string.h>
#include "../bmp_lib/include/image.h"
#include "../bmp_lib/include/bmp.h"
#include "../bmp_lib/include/api.h"
#include "../include/sepia_filter.h"
#include "test.h"
#include "utils.h"

void performance_test(char* src_path) {
    uint16_t path_len = strlen(src_path);
    char log_path[128] = "\0";
    strcat(log_path, src_path);
    strcat(log_path, "_log.txt");

    char img_path[128] = "\0";
    strcat(img_path, src_path);
    strcat(img_path, ".bmp");

    char out_path[128] = "\0";
    strcat(out_path, src_path);
    strcat(out_path, "_out.bmp");

    char out_sse_path[128] = "\0";
    strcat(out_sse_path, src_path);
    strcat(out_sse_path, "sse_out.bmp");

    FILE* log = fopen(log_path, "w");

    struct image img = {0};
    read_from_file(&img, img_path, from_bmp);

    fprintf(log, "Without SSE:\n");
    struct image new_img = test_with_timer(sepia_filter, img, log);
    write_to_file(&new_img, out_path, to_bmp);
    clear_image_memory(&new_img);

    fprintf(log, "With SSE:\n");
    struct image new_img_sse = test_with_timer(sepia_filter_sse, img, log);
    write_to_file(&new_img_sse, out_sse_path, to_bmp);
    clear_image_memory(&new_img_sse);
    clear_image_memory(&img);
    fclose(log);
}