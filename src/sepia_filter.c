//
// Created by vlad on 1/4/23.
//

#include <stdlib.h>
#include <stdio.h>
#include "sepia_filter.h"
#include "../include/image.h"

static void process_pixel(const struct pixel *from, struct pixel *to);

struct image sepia_filter(struct image const source) {
    const struct image new_img = create_image(source.width, source.height);
    if (!new_img.data)
        return new_img;
    const struct pixel* from = source.data;
    struct pixel* to = new_img.data;
    for (size_t i = 0; i < source.width; i++) {
        for (size_t j = 0; j < source.height; j++) {
            process_pixel(from, to);
            from++, to++;
        }
    }
    return new_img;
}

extern void process_pixels_sse(void *ptr1, void *ptr2);

struct image sepia_filter_sse(struct image const source) {
    const struct image new_img = create_image(source.width, source.height);
    if (!new_img.data)
        return new_img;

    size_t img_size = source.width*source.height;
    for (size_t i = 0; i < img_size; i+=4){
        process_pixels_sse(source.data + i, new_img.data + i);
    }
    for (size_t i = img_size - img_size % 4; i < img_size; i++) {
        process_pixel(source.data+i, new_img.data+i);
    }
    return new_img;
}

static void process_pixel(const struct pixel *from, struct pixel *to) {
    ushort tr = (ushort)(0.393 * from->r + 0.769 * from->g + 0.189 * from->b);
    ushort tg = (ushort)(0.349 * from->r + 0.686 * from->g + 0.168 * from->b);
    ushort tb = (ushort)(0.272 * from->r + 0.534 * from->g + 0.131 * from->b);
    to->r = (tr > 255) ? 255 : (uint8_t)tr;
    to->g = (tg > 255) ? 255 : (uint8_t)tg;
    to->b = (tb > 255) ? 255 : (uint8_t)tb;
}
