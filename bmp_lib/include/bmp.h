//
// Created by vlad on 11/6/22.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include <stdio.h>

#include "image.h"
#include "io_statuses.h"

enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, const struct image* img );


#endif //IMAGE_TRANSFORMER_BMP_H
