//
// Created by vlad on 11/6/22.
//
#include "../include/bmp.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool read_header(FILE* in, struct bmp_header* header) {
    if (!fread(header, sizeof(struct bmp_header), 1, in)
            || !header->biHeight || !header->biWidth || !header->bOffBits || !header->biSize
            || header->bfType != 0x4D42)
        return false;
    return true;
}

static bool skip_bmp_padding(FILE* in, uint64_t width) {
    int8_t offset = (int8_t)(width % 4);
    if (offset != 0)
        return fseek(in,  offset, SEEK_CUR) == 0;
    return true;
}

static bool write_bmp_padding(FILE* out, uint64_t width) {
    unsigned char null = 0x0;
    int8_t offset = (int8_t)(width % 4);
    for (uint64_t i = 0; i < offset; i++) {
        if (fwrite(&null, 1, 1, out) != 1)
            return false;
    }
    return true;
}


static enum read_status read_pixels(FILE* in, struct image* img) {
    struct pixel* current = img->data;
    for (size_t i = 0; i < img->height; i++) {
        size_t read = fread(current, sizeof(struct pixel), img->width, in);
        if (read != img->width)
            return READ_INVALID_BITS;
        if (!skip_bmp_padding(in, img->width))
            return READ_INVALID_SIGNATURE;
        current =  current + read;
    }
    return READ_OK;
}


enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    if (!read_header(in, &header)) {
        return READ_INVALID_HEADER;
    }
    *img = create_image(header.biWidth, header.biHeight);
    fseek(in, header.bOffBits, SEEK_SET);
    return read_pixels(in, img);
}

static void fill_header(struct bmp_header* header, struct image const* img) {
    header->bfType = 0x4D42;
    header->bfileSize = sizeof(struct bmp_header) + 3*img->height*img->width + img->height*((3*img->width) % 4);
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->bOffBits = sizeof(struct bmp_header);
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biSize = 40;
    header->biCompression = 0;
    header->biSizeImage = sizeof(struct pixel)*img->height*img->width;
    header->biXPelsPerMeter = 2835;
    header->biYPelsPerMeter = 2835;
}

enum write_status to_bmp( FILE* out, const struct image* img ) {
    struct bmp_header header = {0};
    fill_header(&header, img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;
    struct pixel* current = img->data;
    for (uint64_t i = 0; i < img->height; i++) {
        size_t written = fwrite(current, sizeof(struct pixel), img->width, out);
        if (written != img->width)
            return WRITE_ERROR;
        if (!write_bmp_padding(out, img->width))
            return WRITE_ERROR;
        current += written;
    }
    return WRITE_OK;
}
